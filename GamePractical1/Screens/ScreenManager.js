﻿var showLogs = true;
var keys;
var playerCharacter;

if (showLogs) {
    console.log("ScreenManager");
}

var gameWidth = 800;
var gameHeight = 450;

var backgroundColor = "#000000";
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'KulturGameDev', {
    preload: preload, create: create, update: update });

function preload() {
    if (showLogs) {

        console.log("ScreenManager ­ preload");
    }
    game.load.image('background', 'Pictures/bg.png');
    game.load.atlas('mario_walking', 'Sprites/MarioSprite.png', 'Sprites/sprites.json', Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    

}

function create() {
    if (showLogs)
        console.log("ScreenManager ­ create");
    var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'background');
    PlayerCharacter = new GameObjects.Character();
    PlayerCharacter.init("Mario");
    Background.anchor.setTo(0.5, 0.5);

    keys = game.input.keyboard.createCursorKeys();

}

function update() {

     if (keys.left.isDown) {

         PlayerCharacter.MoveLeft();
    }

    else if (keys.right.isDown)
    {

        PlayerCharacter.MoveRight();
    }

    else if (keys.up.isDown) {

        PlayerCharacter.Jump();

    }

    else {

        PlayerCharacter.Stand();
    }
}